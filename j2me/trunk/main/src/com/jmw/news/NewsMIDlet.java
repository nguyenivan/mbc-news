/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * MOBICOM or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with MOBICOM.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 2, 2011 2:09:23 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */

package com.jmw.news;

import javax.microedition.lcdui.Display;

import la.pandora.mobile.logging.Logger;
import la.pandora.mobile.logging.SystemLogger;

import org.kalmeo.kuix.core.Kuix;
import org.kalmeo.kuix.core.KuixMIDlet;
import org.kalmeo.kuix.widget.Desktop;

import com.jmw.foundation.model.Setting;
import com.jmw.news.foundation.controller.ControllerProvider;
import com.jmw.news.ui.screens.DashBoardFrame;
import com.jmw.updater.Updater;

public class NewsMIDlet extends KuixMIDlet {

  public static Logger logger = new SystemLogger("FunRingMidlet");

  private static NewsMIDlet instance;
  private Display display;

  public static NewsMIDlet getInstance() {
    return instance;
  }

  public void exit() {
    notifyDestroyed();
  }

  public NewsMIDlet() {
    instance = this;
  }

  /**
   * Initialize all styles for application
   */
  public void initDesktopStyles() {
    logger.info("initDesktopStyles");
    Kuix.loadCss("/css/style.css");
  }

  /**
   * Initialize all all contents for application. It's often used to
   * set start up screen
   */
  public void initDesktopContent(Desktop desktop) {
    logger.info("initDesktopContent");
    String autoUpdate =
        ControllerProvider.getSettingController().getAutoUpdate();
    logger.info("autoUpdate=" + autoUpdate);
    if (autoUpdate.equals(Setting.AutoUpdate.NO_PROMPT)
        || autoUpdate.equals(Setting.AutoUpdate.PROMPT)) {
      Updater.start(
          this,
          Prefs.getAppName(),
          autoUpdate.equals(Setting.AutoUpdate.PROMPT),
          false,
          new ShowUpdateResultAlertImpl());
    }
    Kuix.getFrameHandler().pushFrame(new DashBoardFrame());
  }
}
