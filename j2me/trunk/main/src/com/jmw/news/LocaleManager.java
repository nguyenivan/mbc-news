/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.

 * 
 * This software is the confidential and proprietary information of
 * MOBICOM or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with MOBICOM.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 2, 2011 2:09:23 PM 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news;

import com.jmw.news.gen.L10nConstants;
import com.jmw.news.gen.L10nResources;
import com.jmw.utils.MTJLocaleUtils;

/**
 * Internal class
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class LocaleManager {
  public static String Locales[] = { L10nConstants.locales.VI_VI };

  static L10nResources localeResources;

  static {
    setLocale(L10nConstants.locales.VI_VI);
  }

  public static String getString(String key) {
    if (localeResources == null) {
      localeResources = L10nResources.getL10nResources(null);
    }
    return MTJLocaleUtils.convert(localeResources.getString(key));
  }

  public static void setLocale(String locale) {
    localeResources = L10nResources.getL10nResources(locale);
  }
}
