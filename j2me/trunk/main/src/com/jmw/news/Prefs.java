/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * MOBICOM or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with MOBICOM.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 2, 2011 2:09:23 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */

package com.jmw.news;

import la.pandora.mobile.config.Config;
import la.pandora.mobile.config.RMSConfig;
import la.pandora.mobile.logging.Logger;
import la.pandora.mobile.logging.SystemLogger;

import com.jmw.application.Application;

public class Prefs {
  private static Config config;

  public static Logger logger = new SystemLogger("Prefs");

  private static String serverAddress = Application
      .properties()
      .getProperty("service.server");

  public static void setServerAddress(String serverAddress) {
    Prefs.serverAddress = serverAddress;
  }

  public static String getServerAddress() {
    return serverAddress;
  }

  public static void setConfig(Config config) {
    Prefs.config = config;
  }

  public static Config getConfig() {
    if (config == null) {
      config = new RMSConfig();
    }
    return config;
  }

  public static String getAppName() {
    return Application.properties().getProperty("app.name");
  }

  public static String getUpdateLink() {
    return Application.properties().getProperty("jarfile.server");
  }

  public static long getUpdateTimeOfGetProviders() {
    return Long.parseLong(Application.properties().getProperty(
        "update.time.of.get.providers"));
  }

  public static long getUpdateTimeOfGetTopics() {
    return Long.parseLong(Application.properties().getProperty(
        "update.time.of.get.topics"));
  }

  /**
   * @return
   */
  public static String getSendFriendMessageContent() {
    return Application.properties().getProperty(
        "send.friends.message.content");
  }

  public static boolean isReleaseRevision() {
    return "true".equals(Application.properties().getProperty(
        "release"));
  }
}
