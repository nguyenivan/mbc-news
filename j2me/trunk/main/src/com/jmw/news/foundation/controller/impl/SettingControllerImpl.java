/**
 * Copyright (C) 2011 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * Mobicom or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with Mobicom.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 9, 2011 10:25:45 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.foundation.controller.impl;

import com.jmw.news.Prefs;
import com.jmw.news.foundation.controller.DefaultValueFactory;
import com.jmw.news.foundation.controller.SettingController;
import com.jmw.updater.Updater;

/**
 * Implementation of SettingController
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class SettingControllerImpl implements SettingController {

  private static final String NUMBER_ITEMS_OF_FIRST_PAGE =
      "nitems_first_page";
  private static final String NUMBER_ITEMS_PER_LOADING_NEWS_HEADERS =
      "nitems_news_headers";
  private static final String AUTO_UPDATE = "auto_update";

  private static final String FIRST_RUN = "first_run";

  public int getNumberItemOfFirstPage() {
    String s =
        Prefs.getConfig().getProperty(NUMBER_ITEMS_OF_FIRST_PAGE);
    try {
      return Integer.parseInt(s);
    } catch (Exception e) {
      return -1;
    }
  }

  public int getNumberItemPerLoadingNewsHeaders() {
    String s =
        Prefs.getConfig().getProperty(
            NUMBER_ITEMS_PER_LOADING_NEWS_HEADERS);
    try {
      return Integer.parseInt(s);
    } catch (Exception e) {
      return -1;
    }
  }

  public void setNumberItemOfFirstPage(int value) {
    Prefs.getConfig().setProperty(
        NUMBER_ITEMS_OF_FIRST_PAGE,
        value + "");
  }

  public void setNumberItemPerLoadingNewsHeaders(int value) {
    Prefs.getConfig().setProperty(
        NUMBER_ITEMS_PER_LOADING_NEWS_HEADERS,
        value + "");
  }

  public String getAutoUpdate() {
    return Prefs.getConfig().getProperty(AUTO_UPDATE) == null ? DefaultValueFactory.AUTO_UPDATE
        : Prefs.getConfig().getProperty(AUTO_UPDATE);
  }

  public void setAutoUpdate(String value) {
    Prefs.getConfig().setProperty(AUTO_UPDATE, value);
  }

  public boolean isFirstRun() {
    String s = Prefs.getConfig().getProperty(FIRST_RUN);
    boolean isFirst = false;
    if (s == null) {
      isFirst = true;

    } else {
      isFirst = !s.equals(Updater.getVersion());
    }
    if (isFirst) {
      Prefs.getConfig().setProperty(FIRST_RUN, Updater.getVersion());
    }
    return isFirst;
  }

  public boolean isReleaseRevision() {
    return Prefs.isReleaseRevision();
  }

}
