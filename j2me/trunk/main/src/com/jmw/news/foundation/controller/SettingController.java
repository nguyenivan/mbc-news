/**
 * Copyright (C) 2011 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * Mobicom or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with Mobicom.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 9, 2011 10:22:57 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.foundation.controller;

/**
 * Internal Class
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public interface SettingController {
  /**
   * Get number items of first page which display at dash-board
   * 
   * @return number items of first page
   */
  public int getNumberItemOfFirstPage();

  /**
   * Set number items of first page which display at dash-board
   * 
   * @param value
   */
  public void setNumberItemOfFirstPage(int value);

  /**
   * Get number items per loading news headers in topic frame
   * 
   * @return number items per loading news headers
   */
  public int getNumberItemPerLoadingNewsHeaders();

  /**
   * Set number items per loading news headers in topic frame
   * 
   * @param value
   */
  public void setNumberItemPerLoadingNewsHeaders(int value);

  /**
   * Get value of auto update option
   * 
   * @return value of auto update option. If not yet set this value,
   *         it will return {@link DefaultValueFactory#AUTO_UPDATE}
   */
  public String getAutoUpdate();

  /**
   * Set value of auto update option
   * 
   * @param value
   */
  public void setAutoUpdate(String value);

  /**
   * @return
   */
  public boolean isFirstRun();

  /**
   * Check the revision is the release revision to do some stuff about
   * preprocessor, config
   * 
   * @return
   */
  public boolean isReleaseRevision();
}
