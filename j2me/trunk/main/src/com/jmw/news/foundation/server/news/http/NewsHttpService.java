/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * Mobicom or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with Mobicom.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 2, 2011 3:01:51 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.foundation.server.news.http;

import java.io.IOException;
import java.util.Vector;

import la.pandora.mobile.error.GlassboxException;
import la.pandora.mobile.logging.Logger;
import la.pandora.mobile.logging.SystemLogger;

import org.json.me.JSONArray;
import org.json.me.JSONObject;
import org.kalmeo.util.GZIP;

import com.jmw.news.Prefs;
import com.jmw.news.foundation.model.News;
import com.jmw.news.foundation.model.NewsHeader;
import com.jmw.news.foundation.model.Topic;
import com.jmw.news.foundation.server.Service;
import com.jmw.news.foundation.server.news.NewsService;
import com.jmw.utils.HttpUtils;
import com.jmw.utils.NameValuePair;

/**
 * Internal Class
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class NewsHttpService extends Service implements NewsService {

  public static Logger logger = new SystemLogger("NewsHttpService");

  public News getNewsContent(String newsId) throws GlassboxException {
    // HttpRequest request =
    // Application.httpSession().newRequest("get");

    Vector params = new Vector();
    params.addElement(new NameValuePair("news_id", newsId));
    String url =
        HttpUtils.buildURL(
            Prefs.getServerAddress(),
            "get_news_content",
            params);

    logger.info("[getNewsContent] url=" + url);

    String resp = null;
    byte[] content = HttpUtils.loadBytesFromURL(url, gzipHeaders);
    try {
      resp = new String(GZIP.inflate(content));
    } catch (IOException e1) {
      e1.printStackTrace();
      resp = new String(content);
    }

    resp = resp.replace('\n', ' ').trim();
    logger.info("[getNewsContent] resp=" + resp);
    News news = null;
    try {
      JSONObject jobj = new JSONObject(resp);
      JSONArray jimages = jobj.getJSONArray("news_image_url");
      Vector imageUrls = new Vector();
      for (int i = 0; i < jimages.length(); i++) {
        String imageUrl = jimages.getString(i);
        if (imageUrl != null) {
          imageUrls.addElement(imageUrl);
        }
      }
      news =
          new News(
              jobj.getString("news_id"),
              jobj.getString("news_title"),
              null,
              imageUrls,
              jobj.getString("news_content"),
              jobj.optString("news_source"),
              jobj.optString("news_date"));
    } catch (Exception e) {
      e.printStackTrace();
      throw new GlassboxException(e.toString());
    }
    return news;
  }

  public Vector getNewsHeaders(String topicId, int offset, int limit)
      throws GlassboxException {
    // HttpRequest request =
    // Application.httpSession().newRequest("get");
    Vector params = new Vector();
    params.addElement(new NameValuePair("topic_id", topicId));
    if (offset != -1)
      params.addElement(new NameValuePair("offset", offset + ""));
    if (limit != -1)
      params.addElement(new NameValuePair("limit", limit + ""));
    String url =
        HttpUtils.buildURL(
            Prefs.getServerAddress(),
            "get_news_headers",
            params);
    logger.info("[getNewsHeaders] url=" + url);
    Vector newHeaders = new Vector();

    String resp = null;
    byte[] content = HttpUtils.loadBytesFromURL(url, gzipHeaders);
    try {
      resp = new String(GZIP.inflate(content));
    } catch (IOException e1) {
      e1.printStackTrace();
      resp = new String(content);
    }

    resp = resp.replace('\n', ' ').trim();
    logger.info("[getNewsHeaders] resp=" + resp);

    try {
      JSONArray jnewsHeaders = new JSONArray(resp);
      for (int i = 0; i < jnewsHeaders.length(); i++) {
        JSONObject jnewsHeader = jnewsHeaders.getJSONObject(i);
        newHeaders.addElement(new NewsHeader(jnewsHeader
            .getString("news_id"), jnewsHeader
            .getString("news_title"), jnewsHeader
            .getString("news_description"), "("
            + jnewsHeader.getString("news_date") + ")"));
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new GlassboxException(e.toString());
    }
    return newHeaders;
  }

  public Vector getTopics() throws GlassboxException {
    // HttpRequest request =
    // Application.httpSession().newRequest("get");
    String url =
        HttpUtils.buildURL(Prefs.getServerAddress(), "get_topics");
    logger.info("[getTopics] url=" + url);
    Vector topics = new Vector();

    String resp = null;
    byte[] content = HttpUtils.loadBytesFromURL(url, gzipHeaders);
    try {
      resp = new String(GZIP.inflate(content));
    } catch (IOException e1) {
      e1.printStackTrace();
      resp = new String(content);
    }

    resp = resp.replace('\n', ' ').trim();
    logger.info("[getTopics] resp=" + resp);

    try {
      JSONArray jtopics = new JSONArray(resp);
      for (int i = 0; i < jtopics.length(); i++) {
        JSONObject jtopic = jtopics.getJSONObject(i);
        topics.addElement(new Topic(jtopic.getString("id"), jtopic
            .getString("topic")));
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new GlassboxException(e.toString());
    }
    return topics;
  }

}
