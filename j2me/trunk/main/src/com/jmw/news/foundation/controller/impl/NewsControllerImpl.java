/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * MOBICOM or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with MOBICOM.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 2, 2011 2:29:31 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.foundation.controller.impl;

import java.util.Vector;

import la.pandora.mobile.error.GlassboxException;
import la.pandora.mobile.util.TaskCallback;
import la.pandora.mobile.util.TaskOperator;

import com.jmw.news.LocaleManager;
import com.jmw.news.foundation.controller.NewsController;
import com.jmw.news.foundation.model.Feature;
import com.jmw.news.foundation.server.ServiceProvider;
import com.jmw.news.foundation.task.news.GetNewsContentTask;
import com.jmw.news.foundation.task.news.GetNewsHeadersTask;
import com.jmw.news.foundation.task.news.GetTopicsTask;
import com.jmw.news.gen.L10nConstants;
import com.jmw.utils.UUIDUtils;
import com.jmw.utils.VectorUtils;

/**
 * Internal Class
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class NewsControllerImpl implements NewsController {
  GetNewsContentTask getContentTask;

  public void getNewsContent(final String newsId,
      TaskCallback callback) {
    getContentTask = new GetNewsContentTask() {
      public void runTask(TaskOperator taskOperator)
          throws GlassboxException {
        setNews(ServiceProvider.getNewsService().getNewsContent(
            newsId));
        try {
          Thread.sleep(3000);
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
        setOperator(taskOperator);
      }
    };
    TaskOperator.prepare(getContentTask, callback);
  }

  public void getNewsHeaders(final String topicId, final int offset,
      final int limit, TaskCallback callback) {
    TaskOperator.prepare(new GetNewsHeadersTask() {
      public void runTask(TaskOperator taskOperator)
          throws GlassboxException {
        setNewsHeaders(ServiceProvider
            .getNewsService()
            .getNewsHeaders(topicId, offset, limit));
      }
    }, callback);
  }

  public void getTopics(TaskCallback callback) {
    TaskOperator.prepare(new GetTopicsTask() {
      public void runTask(TaskOperator taskOperator)
          throws GlassboxException {
        setTopics(ServiceProvider.getNewsService().getTopics());
      }
    }, callback);

  }

  public Vector getFeatures() {

    return VectorUtils
        .fromArray(new Feature[] {
            new Feature(UUIDUtils.generate(), LocaleManager
                .getString(L10nConstants.keys.TOPIC), Feature.TOPIC),
            new Feature(
                UUIDUtils.generate(),
                LocaleManager.getString(L10nConstants.keys.SETTING),
                Feature.SETTING),
            new Feature(
                UUIDUtils.generate(),
                LocaleManager.getString(L10nConstants.keys.ACCOUNT),
                Feature.ACCOUNT) });
  }

  public void cancelNewsContent() {
    // TODO Auto-generated method stub

  }

}
