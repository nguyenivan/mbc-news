/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * Mobicom or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with Mobicom.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 3, 2011 10:04:02 AM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.foundation.controller;

import com.jmw.news.foundation.controller.impl.ApplicationControllerImpl;
import com.jmw.news.foundation.controller.impl.NewsControllerImpl;
import com.jmw.news.foundation.controller.impl.SettingControllerImpl;

/**
 * Internal Class
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class ControllerProvider {
  private static NewsController newsController;
  private static SettingController settingController;
  private static ApplicationController applicationController;

  public static NewsController getNewsController() {
    if (newsController == null) {
      newsController = new NewsControllerImpl();
    }
    return newsController;
  }

  /**
   * @return the settingController
   */
  public static SettingController getSettingController() {
    if (settingController == null) {
      settingController = new SettingControllerImpl();
    }
    return settingController;
  }

  /**
   * @return the applicationController
   */
  public static ApplicationController getApplicationController() {
    if (applicationController == null) {
      applicationController = new ApplicationControllerImpl();
    }
    return applicationController;
  }

}
