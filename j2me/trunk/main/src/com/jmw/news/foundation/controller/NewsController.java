/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * MOBICOM or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with MOBICOM.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 2, 2011 2:28:53 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.foundation.controller;

import java.util.Vector;

import la.pandora.mobile.util.TaskCallback;

/**
 * Internal Class
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public interface NewsController {
  /**
   * Get list of topics
   * 
   * @param callback
   *          handle result
   */
  public void getTopics(final TaskCallback callback);

  /**
   * Get list of news header of a topic. News header contains id of
   * news, title of news and a header of content
   * 
   * @param topicId
   *          id of topic
   * @param offset
   *          skip how many rows before return result
   * @param limit
   *          number of rows will return
   * @param callback
   *          handle result
   * 
   */
  public void getNewsHeaders(final String topicId, final int offset,
      final int limit, final TaskCallback callback);

  /**
   * Get news content
   * 
   * @param newsId
   * @param callback
   */
  public void getNewsContent(final String newsId,
      final TaskCallback callback);

  /**
   * cancel getting new content from server or other source
   */
  public void cancelNewsContent();

  /**
   * List of feature in Dash Board. Some features is Topic, Account,
   * Setting..
   * 
   * @return List of feature
   */
  public Vector getFeatures();

}
