package com.jmw.news.foundation.server;

public class ServerResultException extends Exception {

  public ServerResultException(String string) {
    super(string);
  }
}
