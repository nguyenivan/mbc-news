package com.jmw.news.foundation.server;

import java.util.Vector;

import com.jmw.utils.NameValuePair;
import com.jmw.utils.VectorUtils;

public class Service {
  protected Service defaultService;
  protected Vector gzipHeaders = VectorUtils
      .fromArray(new NameValuePair[] {
          new NameValuePair("User-Agent", "gzip"),
          new NameValuePair("Accept-Encoding", "gzip") });

  public void setDefaultService(Service service) {
    defaultService = service;
  }
}
