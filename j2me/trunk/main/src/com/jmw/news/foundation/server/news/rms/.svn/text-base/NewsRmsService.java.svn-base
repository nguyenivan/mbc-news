/**
 * Copyright (C) 2011 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * Mobicom or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with Mobicom.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 9, 2011 3:52:41 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.foundation.server.news.rms;

import java.util.Vector;

import la.pandora.mobile.error.GlassboxException;
import la.pandora.mobile.logging.Logger;
import la.pandora.mobile.logging.SystemLogger;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.jmw.news.Prefs;
import com.jmw.news.foundation.cache.CacheManager;
import com.jmw.news.foundation.model.News;
import com.jmw.news.foundation.model.Topic;
import com.jmw.news.foundation.server.Service;
import com.jmw.news.foundation.server.news.NewsService;
import com.jmw.utils.UTF8Util;

/**
 * Internal Class
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class NewsRmsService extends Service implements NewsService {

  private static final String TOPICS_CACHE = "topics";
  public static Logger logger = new SystemLogger("NewsRmsService");

  public News getNewsContent(String newsId) throws GlassboxException {
    // TODO Auto-generated method stub
    return ((NewsService) defaultService).getNewsContent(newsId);
  }

  public Vector getNewsHeaders(String topicId, int offset, int limit)
      throws GlassboxException {
    // TODO Auto-generated method stub
    return ((NewsService) defaultService).getNewsHeaders(
        topicId,
        offset,
        limit);
  }

  Vector getRmsTopics() {
    String tmpContent = Prefs.getConfig().getProperty(TOPICS_CACHE);
    String content = null;
    if (tmpContent != null) {
      content =
          UTF8Util.UTF8Decode(tmpContent.getBytes(), 0, tmpContent
              .getBytes().length);
    }
    logger.info("[getRmsTopics] topics=" + content);
    Vector topics = new Vector();
    try {
      JSONArray jtopics = new JSONArray(content);
      for (int i = 0; i < jtopics.length(); i++) {
        JSONObject jtopic = jtopics.getJSONObject(i);
        topics.addElement(new Topic(jtopic.getString("id"), jtopic
            .getString("topic")));
      }
    } catch (Exception e) {
      return null;
    }
    return topics;
  }

  public Vector getTopics() throws GlassboxException {
    Vector topics = getRmsTopics();
    if (topics == null
        || CacheManager.isExpired("get_topics", Prefs
            .getUpdateTimeOfGetTopics())) {
      if (defaultService != null) {
        topics = ((NewsService) defaultService).getTopics();
        saveTopics(topics);
        CacheManager.saveLastUpdateTime("get_topics", System
            .currentTimeMillis());
      }
    }
    return topics;
  }

  private void saveTopics(Vector topics) {
    JSONArray content = new JSONArray();
    try {
      for (int i = 0; i < topics.size(); i++) {
        Topic topic = (Topic) topics.elementAt(i);
        JSONObject jtopic = new JSONObject();
        jtopic.put("id", topic.getId());
        jtopic.put("topic", topic.getName());
        content.put(jtopic);
      }
      logger.info("saveTopics");
      Prefs.getConfig().setProperty(
          TOPICS_CACHE,
          new String(UTF8Util.UTF8Encode(content.toString())));
    } catch (JSONException e) {
      e.printStackTrace();
    }
  }

}
