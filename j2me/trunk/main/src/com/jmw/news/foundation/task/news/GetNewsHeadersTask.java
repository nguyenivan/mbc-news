/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * Mobicom or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with Mobicom.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 2, 2011 3:49:44 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.foundation.task.news;

import java.util.Vector;

import com.jmw.foundation.task.BaseTask;

/**
 * Internal Class
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public abstract class GetNewsHeadersTask extends BaseTask {
  private Vector newsHeaders;

  public void setNewsHeaders(Vector newsHeaders) {
    this.newsHeaders = newsHeaders;
  }

  public Vector getNewsHeaders() {
    return newsHeaders;
  }

}
