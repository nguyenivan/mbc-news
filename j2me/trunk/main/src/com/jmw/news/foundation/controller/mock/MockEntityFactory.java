/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * Mobicom or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with Mobicom.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 2, 2011 3:12:31 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.foundation.controller.mock;

import java.util.Vector;

import la.pandora.mobile.logging.Logger;
import la.pandora.mobile.logging.SystemLogger;

import com.jmw.news.foundation.model.News;
import com.jmw.news.foundation.model.NewsHeader;
import com.jmw.news.foundation.model.Topic;

/**
 * This class provides
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class MockEntityFactory {
  public static Logger logger = new SystemLogger("MockEntityFactory");

  public static String[] TOPIC_NAMES =
      { "X\u00E3 H\u1ED9i", "Th\u1EBF Gi\u1EDBi", "Kinh Doanh",
          "Th\u1EC3 Thao", "V\u0103n H\u00F3a",
          "Ph\u00E1p Lu\u1EADt", "\u0110\u1EDDi S\u1ED1ng",
          "Khoa H\u1ECDc", "Vi T\u00EDnh", "� T� - Xe M�y" };

  public static String[] NEWS_TITLES =
      {
          "D\u00E2n v\u0103n ph\u00F2ng c\u1EAFt gi\u1EA3m chi ti\u00EAu",
          "\u0110eo b\u00E1m kh\u00E1ch du l\u1ECBch tr\u00EAn v\u1ECBnh H\u1EA1 Long",
          "Bi\u1EC7t k\u00EDch Anh s\u1EB5n s\u00E0ng v\u00E0o Libya",
          "L\u00E3i ti\u1EBFt ki\u1EC7m ti\u1EBFp t\u1EE5c bi\u1EBFn \u0111\u1ED9ng, l\u00EAn tr\u00EAn 17% m\u1ED9t n\u0103m",
          "Minh Ch\u00E2u vui v\u1EDBi n\u1ED7i c\u00F4 \u0111\u01A1n",
          "10 kho\u1EA3nh kh\u1EAFc \u1EA5n t\u01B0\u1EE3ng c\u1EE7a si\u00EAu sao Ryan Giggs" };

  public static String[] NEWS_HEADERS =
      {
          "H\u1EA1n ch\u1EBF bia b\u1ECDt; c\u1EAFt kh\u1EA9u ph\u1EA7n c\u01A1m ti\u1EC7m; chuy\u1EC3n t\u1EEB qu\u00E1n sang tr\u1ECDng ra c\u00E0 ph\u00EA v\u1EC9a h\u00E8; t\u1EE5 t\u1EADp \u0103n u\u1ED1ng ch\u1ECDn m\u00F3n r\u1EBB, b\u1ECF ch\u1EA7u karaoke xa x\u1EC9... l\u00E0 c\u00E1ch m\u00E0 d\u00E2n c\u00F4ng s\u1EDF \u0111\u1ED1i ph\u00F3 v\u1EDBi v\u1EADt gi\u00E1 leo thang. ",
          "\u1EB4m tr\u1EBB em l\u00EAnh \u0111\u00EAnh tr\u00EAn bi\u1EC3n r\u1ED3i \u1EADp t\u1EDBi hai b\u00EAn th\u00E0nh t\u00E0u ng\u1EEDa tay xin ti\u1EC1n, nh\u1EEFng ng\u01B0\u1EDDi h\u00E0nh ngh\u1EC1 \u0111eo b\u00E1m kh\u00E1ch du l\u1ECBch tr\u00EAn v\u1ECBnh H\u1EA1 Long (Qu\u1EA3ng Ninh) \u0111ang xu\u1EA5t hi\u1EC7n ng\u00E0y m\u1ED9t \u0111\u00F4ng.",
          "L\u1EF1c l\u01B0\u1EE3ng \u0111\u1EB7c nhi\u1EC7m Anh (SAS) \u0111\u01B0\u1EE3c cho l\u00E0 \u0111ang chu\u1EA9n b\u1ECB tung v\u00E0o Libya \u0111\u1EC3 chi\u1EBFm nh\u1EEFng kho ch\u1EE9a kh\u00ED \u0111\u1ED9c c\u00F3 th\u1EC3 tr\u1EDF th\u00E0nh v\u0169 kh\u00ED hu\u1EF7 di\u1EC7t do ch\u00EDnh quy\u1EC1n \u0111\u1EA1i t\u00E1 Gadhafi c\u1EA5t gi\u1EA5u t\u1EA1i v\u00F9ng sa m\u1EA1c.",
          "V\u1EDBi kho\u1EA3n ti\u1EC1n g\u1EEDi t\u1EEB m\u1ED9t t\u1EF7 \u0111\u1ED3ng tr\u1EDF l\u00EAn, m\u1ED9t nh\u00E0 b\u0103ng c\u1ED5 ph\u1EA7n cho kh\u00E1ch h\u00E0ng h\u01B0\u1EDFng l\u00E3i su\u1EA5t tr\u00EAn 17% m\u1ED9t n\u0103m. M\u1ED9t s\u1ED1 nh\u00E0 b\u0103ng kh\u00E1c c\u0169ng \u0111\u01B0a l\u00E3i su\u1EA5t l\u00EAn x\u1EA5p x\u1EC9 m\u1EE9c n\u00E0y \u0111\u1EC3 gi\u1EEF kh\u00E1ch.",
          "Ch\u1ECB k\u1EC3, c\u00F3 ng\u01B0\u1EDDi \u0111\u00E0n \u00F4ng t\u1EEBng n\u00F3i ch\u1EBFt ch\u00ECm trong \u0111\u00F4i m\u1EAFt ch\u1ECB. Ng\u01B0\u1EDDi tinh \u00FD ch\u1EC9 c\u1EA7n nh\u00ECn v\u00E0o \u0111\u00F4i m\u1EAFt \u1EA5y l\u00E0 c\u00F3 th\u1EC3 \u0111\u1ECDc \u0111\u01B0\u1EE3c t\u00EDnh c\u00E1ch v\u00E0 cu\u1ED9c \u0111\u1EDDi Minh Ch\u00E2u - m\u1ED9t \u0111\u00F4i m\u1EAFt to, th\u0103m th\u1EB3m, c\u1EA5t gi\u1EA5u trong n\u00F3 nh\u1EEFng y\u1EBFu \u0111u\u1ED1i v\u00E0 m\u1EA1nh m\u1EBD.",
          "T\u00EDnh \u0111\u1EBFn tr\u1EADn g\u1EB7p Chelsea t\u1ED1i th\u1EE9 ba, ti\u1EC1n v\u1EC7 c\u00E1nh t\u00E0i hoa Ryan Giggs \u0111\u00E3 c\u00F3 tr\u00F2n 20 n\u0103m ch\u01A1i cho MU." };
  public static String NEWS_CONTENT =
      "L\u1EABn trong bi\u1EC3n ng\u01B0\u1EDDi \u1EDF tr\u1EA1i t\u1ECB n\u1EA1n bi\u00EAn gi\u1EDBi Ras Jdir, ch\u00FAng t\u00F4i b\u1EAFt g\u1EB7p hai lao \u0111\u1ED9ng trong t\u00ECnh c\u1EA3nh m\u00E0 Th\u1EE9 tr\u01B0\u1EDFng B\u1ED9 Ngo\u1EA1i giao \u0110o\u00E0n Xu\u00E2n H\u01B0ng g\u1ECDi l\u00E0 \u201Ct\u00ECnh tr\u1EA1ng \u0111\u1EB7c bi\u1EC7t\u201D. Trong \u0111\u00F3, bi k\u1ECBch nh\u1EA5t l\u00E0 lao \u0111\u1ED9ng S\u00F9ng A Khua (\u1EDF Y\u00EAn B\u00E1i) n\u1EB1m d\u1EB7t d\u1EB9o trong v\u00F2ng tay ng\u01B0\u1EDDi anh v\u00E0 \u0111\u1EE9a em c\u00F9ng \u0111i xu\u1EA5t kh\u1EA9u lao \u0111\u1ED9ng \u1EDF Libya. C\u00E2u chuy\u1EC7n ba anh em k\u1EC3 khi\u1EBFn ai nghe c\u0169ng x\u00F3t l\u00F2ng cho s\u1ED1 ph\u1EADn c\u1EE7a h\u1ECD. Khua c\u00F9ng v\u1EDBi hai anh em c\u1EE7a m\u00ECnh do C\u00F4ng ty Vinaconex Mex \u0111\u01B0a qua Libya l\u00E0m vi\u1EC7c v\u1EEBa b\u1ED1n th\u00E1ng th\u00EC g\u1EB7p ph\u1EA3i t\u00ECnh c\u1EA3nh ch\u1EA1y lo\u1EA1n n\u00E0y. Trong m\u1ED9t l\u1EA7n b\u1ECB s\u1ED1t khi l\u00E0m vi\u1EC7c, Khua th\u1EA5y t\u00EA t\u00EA ch\u00E2n n\u00EAn \u0111\u01B0\u1EE3c ch\u1EE7 s\u1EED d\u1EE5ng lao \u0111\u1ED9ng H\u00E0n Qu\u1ED1c \u0111\u01B0a \u0111i kh\u00E1m v\u00E0 ch\u1EE7 quan cho r\u1EB1ng ch\u1EC9 b\u1ECB nh\u1EB9. V\u1EADy m\u00E0 20 ng\u00E0y sau c\u01A1n s\u1ED1t c\u00E0ng n\u1EB7ng v\u00E0 \u0111\u1EA9y Khua v\u00E0o t\u00ECnh tr\u1EA1ng li\u1EC7t n\u1EEDa ng\u01B0\u1EDDi. B\u1ECB li\u1EC7t v\u00E0i ng\u00E0y c\u0169ng l\u00E0 l\u00FAc Tripoli vang l\u00EAn ti\u1EBFng s\u00FAng b\u1EA1o lo\u1EA1n, c\u1EA3 l\u00E1n tr\u1EA1i h\u1ED1t ho\u1EA3ng t\u00ECm m\u1ECDi c\u00E1ch \u0111\u1EC3 tho\u00E1t th\u00E2n. \u201CT\u00ECnh c\u1EA3nh l\u00FAc \u1EA5y ai c\u0169ng ho\u1EA3ng lo\u1EA1n v\u00EC c\u01B0\u1EDBp b\u00F3c, ti\u1EBFng s\u00FAng ng\u00E0y m\u1ED9t g\u1EA7n v\u1EDBi l\u00E1n tr\u1EA1i. T\u1EA5t c\u1EA3 ch\u00FAng t\u00F4i v\u01A1 v\u1ED9i \u00E1o qu\u1EA7n v\u00E0 nh\u1EEFng th\u1EE9 c\u1EA7n thi\u1EBFt \u0111\u1EC3 di t\u1EA3n. Em t\u00F4i b\u1ECB li\u1EC7t van n\u00E0i: C\u1EE9 b\u1ECF em l\u1EA1i \u0111\u00E2y, anh c\u1ED1 ch\u1EA1y \u0111i. \u0110\u1EDDi em coi nh\u01B0 \u0111\u00E3 ch\u1EBFt r\u1ED3i, n\u1EBFu mang em theo c\u00F3 khi ch\u1EBFt ch\u00F9m th\u00EC ba m\u1EB9 \u1EDF nh\u00E0 sao ch\u1ECBu th\u1EA5u!\u201D - ng\u01B0\u1EDDi anh S\u00F9ng A C\u00E2u k\u1EC3 l\u1EA1i. Nh\u01B0ng l\u00E0m sao c\u00F3 th\u1EC3 b\u1ECF l\u1EA1i \u0111\u1EE9a em trong ho\u00E0n c\u1EA3nh n\u00E0y d\u00F9 n\u00F3 b\u1ECB li\u1EC7t, hai anh em S\u00F9ng A C\u00E2u v\u00E0 c\u00E1c lao \u0111\u1ED9ng kh\u00E1c gi\u00FAp b\u1ED3ng S\u00F9ng A Khua ch\u1EA1y kh\u1ECFi l\u00E1n tr\u1EA1i. May thay, \u00F4ng ch\u1EE7 H\u00E0n Qu\u1ED1c \u0111\u00E3 l\u1EA5y \u00F4t\u00F4 ch\u1EDF nh\u00F3m lao \u0111\u1ED9ng n\u00E0y ra bi\u00EAn gi\u1EDBi Tunisia \u0111\u1EC3 giao l\u1EA1i cho t\u1ED5 ch\u1EE9c t\u1EEB thi\u1EC7n c\u1EE7a Li\u00EAn Hi\u1EC7p Qu\u1ED1c \u0111ang l\u1EADp tr\u1EA1i t\u1ECB n\u1EA1n c\u1EE9u ng\u01B0\u1EDDi \u1EDF bi\u00EAn gi\u1EDBi. C\u00F9ng chung nh\u00F3m ch\u1EA1y lo\u1EA1n c\u00F2n c\u00F3 anh Phan Quang \u1EE6y (huy\u1EC7n Can L\u1ED9c, H\u00E0 T\u0129nh) t\u00ECm \u0111\u01B0\u1EDDng tho\u00E1t kh\u1ECFi Libya trong t\u00ECnh c\u1EA3nh ch\u00E2n ph\u1EA3i b\u1ECB g\u00E3y. \u1EE6y b\u1ECB r\u01A1i xu\u1ED1ng t\u1EEB l\u1EA7u 4 khi \u0111ang l\u00E0m vi\u1EC7c v\u00EC b\u01B0\u1EDBc h\u1EE5t ch\u00E2n tr\u00EAn gi\u00E0n gi\u00E1o. \u0110\u01B0\u1EE3c \u0111\u01B0a v\u00E0o b\u1EC7nh vi\u1EC7n kh\u00E1m nh\u01B0ng kh\u00F4ng \u0111\u01B0\u1EE3c b\u00F3 b\u1ED9t. N\u1EB1m tr\u1EA1i c\u1EA3 tu\u1EA7n, c\u00E1i ch\u00E2n g\u00E3y \u0111ang b\u1ECB s\u01B0ng to \u0111au nh\u1EE9c th\u00EC b\u1EA1o lo\u1EA1n x\u1EA3y ra, anh \u1EE6y ngh\u0129 phen n\u00E0y ch\u1EAFc ch\u1EBFt. May thay ng\u01B0\u1EDDi ch\u1EE7 l\u1EA5y \u00F4t\u00F4 \u0111\u01B0a anh ra bi\u00EAn gi\u1EDBi giao cho c\u00E1c t\u1ED5 ch\u1EE9c t\u1EEB thi\u1EC7n \u1EDF tr\u1EA1i t\u1ECB n\u1EA1n. Ch\u1EC9 khi \u1EDF \u0111\u00E2y anh \u1EE6y m\u1EDBi \u0111\u01B0\u1EE3c b\u00F3 b\u1ED9t v\u00E0 c\u00F3 m\u1ED9t \u00EDt thu\u1ED1c gi\u1EA3m \u0111au \u0111\u1EC3 u\u1ED1ng, v\u00E0 anh cho bi\u1EBFt ph\u1EA3i n\u1EB1m \u1EDF tr\u1EA1i t\u1ECB n\u1EA1n h\u01A1n 10 ng\u00E0y nay.";

  public static Vector createTopics() {
    Vector topics = new Vector();
    for (int i = 0; i < TOPIC_NAMES.length; i++) {
      topics.addElement(new Topic(
          System.currentTimeMillis() + "",
          TOPIC_NAMES[i]));
    }
    return topics;
  }

  /**
   * @return
   */
  public static Vector createNewHeaders(int count) {
    Vector newsHeaders = new Vector();
    for (int i = 0; i < (NEWS_TITLES.length < count ? NEWS_TITLES.length
        : count); i++) {
      newsHeaders.addElement(new NewsHeader(System
          .currentTimeMillis()
          + "", NEWS_TITLES[i], NEWS_HEADERS[i], "date"));
    }
    return newsHeaders;
  }

  /**
   * @return
   */
  public static News createNewContent() {
    // TODO Auto-generated method stub
    return new News(
        System.currentTimeMillis() + "",
        NEWS_TITLES[0],
        NEWS_HEADERS[0],
        null,
        NEWS_CONTENT,
        null,
        null);
  }
}
