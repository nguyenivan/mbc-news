package com.jmw.news.foundation.server;

import com.jmw.application.Application;

public class SessionToken {
  protected static String auth;
  protected static String accountId;
  protected static String username;
  protected static String displayName;
  protected static String phoneNumber = Application.properties()
      .getProperty("phone.number");
  private static String telco;
  private static String telcoId;

  public static String getAuth() {
    return auth;
  }

  public static void setAuth(String auth) {
    SessionToken.auth = auth;
  }

  public static String getAccountId() {
    return accountId;
  }

  public static void setAccountId(String accountId) {
    SessionToken.accountId = accountId;
  }

  public static String getUsername() {
    return username;
  }

  public static void setUsername(String username) {
    SessionToken.username = username;
  }

  public static String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    SessionToken.displayName = displayName;
  }

  public static String getPhoneNumber() {
    return phoneNumber;
  }

  public static void setPhoneNumber(String phoneNumber) {
    SessionToken.phoneNumber = phoneNumber;
  }

  public static void setTelco(String telco) {
    SessionToken.telco = telco;
  }

  public static String getTelco() {
    return telco;
  }

  public static void setTelcoId(String telcoId) {
    SessionToken.telcoId = telcoId;
  }

  public static String getTelcoId() {
    return telcoId;
  }

  public static String getPlatform() {
    return "j2me";
  }

}
