package com.jmw.news.foundation.server;

import com.jmw.foundation.server.jmw.JMWService;
import com.jmw.foundation.server.jmw.http.JMWHttpService;
import com.jmw.news.foundation.server.news.NewsService;
import com.jmw.news.foundation.server.news.http.NewsHttpService;
import com.jmw.news.foundation.server.news.rms.NewsRmsService;

public class ServiceProvider {
  private static JMWService versionService;
  private static NewsService newsService;

  public static JMWService getVersionService() {
    if (versionService == null) {
      versionService = new JMWHttpService();
    }
    return versionService;
  }

  public static NewsService getNewsService() {
    if (newsService == null) {
      NewsRmsService rmsService = new NewsRmsService();
      rmsService.setDefaultService(new NewsHttpService());
      newsService = rmsService;
    }
    return newsService;
  }
}
