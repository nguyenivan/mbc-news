package com.jmw.news.gen;
/**
 *	L10nConstants class is auto generated. DO NOT CHANGE IT.
 */
public class L10nConstants {
	
    /**
 	 *	LOCALE NAMES
 	 */
    public class locales {
		public static final String VI_VI = "vi-VI";

    }

    /**
 	 *	KEY VALUES
 	 */    
    public class keys {
		public static final String NEWS = "NEWS";
		public static final String PROMPT_NO_NEW_VERSION = "PROMPT_NO_NEW_VERSION";
		public static final String PROMPT_HAS_NEW_VERSION = "PROMPT_HAS_NEW_VERSION";
		public static final String SEE_MORE = "SEE_MORE";
		public static final String SELECT = "SELECT";
		public static final String SEND_TO_FRIEND = "SEND_TO_FRIEND";
		public static final String UPDATE_VERSION = "UPDATE_VERSION";
		public static final String ABOUT = "ABOUT";
		public static final String BACK = "BACK";
		public static final String MENU = "MENU";
		public static final String EXIT = "EXIT";
		public static final String FIRST_PAGE = "FIRST_PAGE";
		public static final String PREFERENCES = "PREFERENCES";
		public static final String TOPIC = "TOPIC";
		public static final String SETTING = "SETTING";
		public static final String FREE_ACCOUNT = "FREE_ACCOUNT";
		public static final String ACCOUNT = "ACCOUNT";
		public static final String NUMBER_ITEMS_OF_FIRST_PAGE = "NUMBER_ITEMS_OF_FIRST_PAGE";
		public static final String NUMBER_ITEMS_PER_LOADING_NEWS_HEADERS = "NUMBER_ITEMS_PER_LOADING_NEWS_HEADERS";
		public static final String APPLICATION_NAME = "APPLICATION_NAME";
		public static final String VERSION = "VERSION";
		public static final String PROMPT_TO_EXIT = "PROMPT_TO_EXIT";
		public static final String AUTO_UPDATE = "AUTO_UPDATE";
		public static final String AUTO_UPDATE_NO_PROMPT = "AUTO_UPDATE_NO_PROMPT";
		public static final String AUTO_UPDATE_PROMPT = "AUTO_UPDATE_PROMPT";
		public static final String NO_AUTO_UPDATE = "NO_AUTO_UPDATE";
		public static final String FOLLOW = "FOLLOW";
		public static final String APP_NAME = "APP_NAME";
		public static final String SEE_MORE_PIC_LABEL = "SEE_MORE_PIC_LABEL";
		public static final String PICTURE = "PICTURE";
		public static final String GO_TOP = "GO_TOP";

    }
}
