/**
 * Copyright (C) 2011 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * Mobicom or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with Mobicom.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 9, 2011 9:03:52 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.ui.screens.provider;

import com.jmw.news.LocaleManager;
import com.jmw.news.gen.L10nConstants;
import com.jmw.ui.foundation.provider.BaseScreenProvider;
import com.jmw.utils.VectorUtils;

/**
 * Internal Class
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class SettingFrameProvider extends BaseScreenProvider {

  private String numberItemsOfFirstPage = LocaleManager
      .getString(L10nConstants.keys.NUMBER_ITEMS_OF_FIRST_PAGE);
  private String numberItemsPerLoadingNewsHeaders =
      LocaleManager
          .getString(L10nConstants.keys.NUMBER_ITEMS_PER_LOADING_NEWS_HEADERS);
  private String autoUpdateLabel = LocaleManager
      .getString(L10nConstants.keys.AUTO_UPDATE);

  private String noPromptLabel = LocaleManager
      .getString(L10nConstants.keys.AUTO_UPDATE_NO_PROMPT);
  private String promptLabel = LocaleManager
      .getString(L10nConstants.keys.AUTO_UPDATE_PROMPT);
  private String noUpdateLabel = LocaleManager
      .getString(L10nConstants.keys.NO_AUTO_UPDATE);

  public final static String REF_NUMBER_ITEMS_OF_FIRST_PAGE_LABEL =
      "ref_number_items_of_first_page_label";
  public final static String REF_NUMBER_ITEMS_PER_LOADING_NEWS_HEADERS_LABEL =
      "ref_number_items_per_loading_news_headers_label";
  public final static String REF_AUTO_UPDATE_LABEL =
      "ref_auto_update_label";
  public final static String REF_AUTO_UPDATE_NO_PROMPT_LABEL =
      "ref_auto_update_no_prompt_label";
  public final static String REF_AUTO_UPDATE_PROMPT_LABEL =
      "ref_auto_update_prompt_label";
  public final static String REF_NO_AUTO_UPDATE_LABEL =
      "ref_no_auto_update_label";

  protected Object getUserDefinedValue(String ref) {
    return getUserDefinedValue(
        ref,
        VectorUtils.fromArray(new String[] {
            REF_NUMBER_ITEMS_OF_FIRST_PAGE_LABEL,
            REF_NUMBER_ITEMS_PER_LOADING_NEWS_HEADERS_LABEL,
            REF_AUTO_UPDATE_LABEL, REF_AUTO_UPDATE_NO_PROMPT_LABEL,
            REF_AUTO_UPDATE_PROMPT_LABEL, REF_NO_AUTO_UPDATE_LABEL }),
        VectorUtils.fromArray(new String[] { numberItemsOfFirstPage,
            numberItemsPerLoadingNewsHeaders, autoUpdateLabel,
            noPromptLabel, promptLabel, noUpdateLabel }));
  }
}
