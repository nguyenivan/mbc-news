/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * Mobicom or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with Mobicom.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 3, 2011 10:21:59 AM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.ui.screens.utils;

import com.jmw.news.foundation.model.Feature;
import com.jmw.news.foundation.model.NewsHeader;
import com.jmw.news.foundation.model.Topic;

/**
 * Internal Class
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class IconPath {

  /**
   * @param topic
   * @return
   */
  public static String getPath(Topic topic) {
    return "news/ic_topic";
  }

  /**
   * @param header
   * @return
   */
  public static String getPath(NewsHeader header) {
    // TODO Auto-generated method stub
    return "news/ic_news";
  }

  /**
   * @param feature
   * @return
   */
  public static String getPath(Feature feature) {
    // TODO Auto-generated method stub
    String path = null;
    if (feature.getType().equals(Feature.TOPIC)) {
      path = "news/ic_topic";
    } else if (feature.getType().equals(Feature.SETTING)) {
      path = "news/ic_setting";
    } else if (feature.getType().equals(Feature.ACCOUNT)) {
      path = "news/ic_account";
    }
    return path;
  }

}
