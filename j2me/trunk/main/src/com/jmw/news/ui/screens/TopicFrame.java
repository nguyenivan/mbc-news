/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * Mobicom or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with Mobicom.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 7, 2011 7:35:34 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.ui.screens;

import java.util.Vector;

import la.pandora.mobile.logging.Logger;
import la.pandora.mobile.logging.SystemLogger;
import la.pandora.mobile.util.TaskCallback;
import la.pandora.mobile.util.TaskOperator;
import la.pandora.mobile.util.TaskResult;

import org.kalmeo.kuix.core.Kuix;
import org.kalmeo.kuix.widget.ListItem;

import com.jmw.foundation.task.TaskUtils;
import com.jmw.news.LocaleManager;
import com.jmw.news.foundation.controller.ControllerProvider;
import com.jmw.news.foundation.model.Topic;
import com.jmw.news.foundation.task.news.GetTopicsTask;
import com.jmw.news.gen.L10nConstants;
import com.jmw.news.ui.screens.provider.TopicFrameProvider;
import com.jmw.news.ui.screens.utils.IconPath;
import com.jmw.ui.foundation.provider.BaseListScreenProvider;
import com.jmw.ui.foundation.provider.IconProvider;
import com.jmw.ui.foundation.screens.BaseListFrame;
import com.jmw.ui.foundation.screens.FrameUtils;
import com.jmw.utils.HashtableUtils;
import com.jmw.utils.DelayedTask;
import com.jmw.utils.SysUtils;
import com.jmw.utils.ThreadUtils;

/**
 * <p>
 * Display list of topics of news. List of topics is get from server
 * or cached. Cached expired time is configured in app.properties
 * file.
 * <p>
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class TopicFrame extends BaseListFrame {
  public static Logger logger = new SystemLogger("TopicFrame");
  public Vector topics;

  /**
   * Internal Class
   * 
   * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
   * 
   */
  public class GetTopicsCallback implements TaskCallback {
    public void taskCallback(TaskResult result) {
      if (TaskUtils.finishProcessFromServer(result.getResultStatus())) {
        logger.info("stop loading topics");
        if (provider != null) {
          ((BaseListScreenProvider) provider)
              .setLoadingVisible(false);
        }
      }
      if (result.getResultStatus() == TaskOperator.STATUS_SUCCESS) {
        GetTopicsTask task = (GetTopicsTask) result.getTask();
        topics = task.getTopics();
        renderView();
      }
    }
  }

  protected void chooseItem() {
    if (getCurrentWidget() instanceof ListItem) {
      ListItem listItem = (ListItem) getCurrentWidget();
      if (listItem.getDataProvider() instanceof IconProvider) {
        IconProvider iconProvider =
            (IconProvider) listItem.getDataProvider();
        if (iconProvider != null) {
          Topic topic = (Topic) iconProvider.getAttachedObj();
          if (topic != null) {
            NewsHeadersFrame newsHeadersFrame =
                new NewsHeadersFrame(
                    HashtableUtils.fromArray(new Object[] {
                        NewsHeadersFrame.EXTRA_NAME, topic.getName(),
                        NewsHeadersFrame.EXTRA_ID, topic.getId() }));
            FrameUtils.removeAndPushFrame(newsHeadersFrame);
          }
        }
      }
    }
  }

  public void onAdded() {
    logger.info("[onAdded] free memory="
        + Runtime.getRuntime().freeMemory());
    Kuix.getCanvas().setInteractionListener(this);
    setScreen(Kuix.loadScreen(
        "/xml/jmw/news/topics_frame.xml",
        provider));
    getScreen().setTitle(
        LocaleManager.getString(L10nConstants.keys.TOPIC));
    SysUtils.checkNotNull(new Object[] { getScreen(), provider });
    getScreen().setCurrent();
    ThreadUtils.runTaskDelayedAt(new DelayedTask() {
      public void run() {
        if (topics == null) {
          ((BaseListScreenProvider) provider).setLoadingVisible(true);
        }
      }
    },
        500);
  }

  public TopicFrame() {
    provider = new TopicFrameProvider();
    ControllerProvider.getNewsController().getTopics(
        new GetTopicsCallback());
  }

  public void renderView() {
    if (topics != null) {
      provider.removeAllItems(TopicFrameProvider.REF_TOPICS);
      for (int i = 1; i < topics.size(); i++) {
        Topic topic = (Topic) topics.elementAt(i);
        IconProvider itemProvider =
            new IconProvider(
                topic.getName(),
                topic.getId(),
                IconPath.getPath(topic),
                topic);
        provider.addItem(TopicFrameProvider.REF_TOPICS, itemProvider);
      }
    }
    if (getScreen() != null) {
      getScreen().invalidateAppearance();
      getScreen().invalidate();
    }
    super.renderView();
  }

}
