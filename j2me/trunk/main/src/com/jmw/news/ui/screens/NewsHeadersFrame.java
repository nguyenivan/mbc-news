/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * Mobicom or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with Mobicom.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 3, 2011 8:43:57 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.ui.screens;

import java.util.Hashtable;
import java.util.Vector;

import la.pandora.mobile.logging.Logger;
import la.pandora.mobile.logging.SystemLogger;
import la.pandora.mobile.util.TaskCallback;
import la.pandora.mobile.util.TaskOperator;
import la.pandora.mobile.util.TaskResult;

import org.kalmeo.kuix.core.Kuix;
import org.kalmeo.kuix.widget.ListItem;

import com.jmw.foundation.task.TaskUtils;
import com.jmw.news.foundation.controller.ControllerProvider;
import com.jmw.news.foundation.controller.DefaultValueFactory;
import com.jmw.news.foundation.model.NewsHeader;
import com.jmw.news.foundation.task.news.GetNewsHeadersTask;
import com.jmw.news.ui.screens.provider.NewsHeaderProvider;
import com.jmw.news.ui.screens.provider.NewsHeadersFrameProvider;
import com.jmw.news.ui.screens.utils.IconPath;
import com.jmw.ui.foundation.provider.BaseListScreenProvider;
import com.jmw.ui.foundation.screens.BaseListFrame;
import com.jmw.ui.foundation.screens.FrameUtils;
import com.jmw.utils.HashtableUtils;
import com.jmw.utils.DelayedTask;
import com.jmw.utils.SysUtils;
import com.jmw.utils.ThreadUtils;

/**
 * <p>
 * This class will display list of headers of news relate to this
 * topic. Number of headers is loaded with default number which config
 * in SettingScreen. User can press at "Load More" button to get more
 * header
 * </p>
 * 
 * <p>
 * How to call this screen
 * 
 * <pre>
 * Hashtable bundle = new Hashtable();
 * bundle.put(TopicFrame.EXTRA_NAME, topicName);
 * bundle.put(TopicFrame.EXTRA_ID, topicId);
 * TopicFrame topicFrame = new TopicFrame();
 * topicFrame.setBundle(bundle);
 * FrameUtils.cleanAndPushFrame(topicFrame);
 * </pre>
 * 
 * With <code>topicName</code> is name of topic, <code>topicId</code>
 * is Id of topic
 * </p>
 * 
 * 
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class NewsHeadersFrame extends BaseListFrame {

  public Vector newsHearders;

  /**
   * Internal Class
   * 
   * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
   * 
   */
  public class GetNewHeardersCallback implements TaskCallback {

    public void taskCallback(TaskResult result) {
      if (provider != null) {
        if (TaskUtils.finishProcessFromServer(result
            .getResultStatus())) {
          logger.info("stop loading new headers");
          ((BaseListScreenProvider) provider)
              .setLoadingVisible(false);
        }
        if (result.getResultStatus() == TaskOperator.STATUS_SUCCESS) {
          GetNewsHeadersTask task =
              (GetNewsHeadersTask) result.getTask();
          newsHearders = task.getNewsHeaders();
          if (provider != null) {
            ((BaseListScreenProvider) provider)
                .setSeeMoreVisible(newsHearders.size() >= numberNewsHeadersPerLoading);
            setPage(getPage() + 1);
            renderView();
          }
        }
      }
    }
  }

  public static Logger logger = new SystemLogger("TopicFrame");

  public static final String EXTRA_NAME = "name";
  public static final String EXTRA_ID = "id";

  private String topicName;
  private String topicId;
  int numberNewsHeadersPerLoading;

  protected void chooseItem() {
    if (getCurrentWidget() instanceof ListItem) {
      ListItem listItem = (ListItem) getCurrentWidget();
      NewsHeaderProvider newsHeaderProvider =
          (NewsHeaderProvider) listItem.getDataProvider();
      if (newsHeaderProvider != null) {
        NewsHeader newsHeader = newsHeaderProvider.getNewsHeader();
        if (newsHeader != null) {
          NewsDetailFrame newsDetailFrame =
              new NewsDetailFrame(
                  HashtableUtils.fromArray(new Object[] {
                      NewsDetailFrame.EXTRA_NAME_HEADER, newsHeader,
                      NewsDetailFrame.EXTRA_TOPIC_NAME, topicName }));
          FrameUtils.removeAndPushFrame(newsDetailFrame);
        }
      }
    }
  }

  public NewsHeadersFrame() {
    this(null);
  }

  /**
   * @param fromArray
   */
  public NewsHeadersFrame(Hashtable bundle) {
    provider = new NewsHeadersFrameProvider();
    if (bundle != null) {
      topicName = (String) bundle.get(EXTRA_NAME);
      topicId = (String) bundle.get(EXTRA_ID);
    }
    if (topicId != null) {

      numberNewsHeadersPerLoading =
          ControllerProvider
              .getSettingController()
              .getNumberItemPerLoadingNewsHeaders() == -1 ? DefaultValueFactory.NEWS_HEARDERS_PER_LOAD
              : ControllerProvider
                  .getSettingController()
                  .getNumberItemPerLoadingNewsHeaders();
      ControllerProvider.getNewsController().getNewsHeaders(
          topicId,
          0,
          numberNewsHeadersPerLoading,
          new GetNewHeardersCallback());
    }
  }

  public void onAdded() {
    logger.info("[onAdded] free memory="
        + Runtime.getRuntime().freeMemory());
    Kuix.getCanvas().setInteractionListener(this);
    setScreen(Kuix.loadScreen(
        "/xml/jmw/news/news_headers_frame.xml",
        provider));
    getScreen().setTitle(topicName);
    SysUtils.checkNotNull(new Object[] { getScreen(), provider });
    getScreen().setCurrent();
    ThreadUtils.runTaskDelayedAt(new DelayedTask() {
      public void run() {
        if (newsHearders == null) {
          ((BaseListScreenProvider) provider).setLoadingVisible(true);
          ((BaseListScreenProvider) provider)
              .setSeeMoreVisible(false);
        }
      }
    },
        500);
    renderView();
  }

  public boolean onMessage(Object identifier, Object[] arguments) {
    logger.info("message=" + identifier);
    return super.onMessage(identifier, arguments);
  }

  public void renderView() {
    if (newsHearders != null) {
      for (int i = 0; i < newsHearders.size(); i++) {
        NewsHeader header = (NewsHeader) newsHearders.elementAt(i);
        NewsHeaderProvider newsHeaderProvider =
            new NewsHeaderProvider(header, IconPath.getPath(header));
        provider.addItem(
            NewsHeadersFrameProvider.REF_NEW_HEADERS,
            newsHeaderProvider);
      }
    }
    super.renderView();
  }

  protected void seeMore() {
    ((NewsHeadersFrameProvider) provider).setLoadingVisible(true);
    ((NewsHeadersFrameProvider) provider).setSeeMoreVisible(false);
    ControllerProvider.getNewsController().getNewsHeaders(
        topicId,
        getPage() * numberNewsHeadersPerLoading,
        numberNewsHeadersPerLoading,
        new GetNewHeardersCallback());
  }

  public void onRemoved() {
    newsHearders = null;
    provider = null;
    super.onRemoved();
  }

}
