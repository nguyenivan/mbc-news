/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * MOBICOM or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with MOBICOM.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 2, 2011 2:09:23 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */

package com.jmw.news.ui.screens.provider;

import com.jmw.ui.foundation.provider.BaseListScreenProvider;
import com.jmw.utils.VectorUtils;

public class NewsHeadersFrameProvider extends BaseListScreenProvider {

  /**
   * Reference to list of news headers in XML layout file
   */

  public final static String REF_NEW_HEADERS = "ref_new_headers";

  protected Object getUserDefinedValue(String ref) {
    return getUserDefinedValue(ref, VectorUtils
        .fromArray(new String[] {}), VectorUtils
        .fromArray(new String[] {}));
  }

}
