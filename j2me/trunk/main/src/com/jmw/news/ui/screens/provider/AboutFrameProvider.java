/**
 * Copyright (C) 2011 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * Mobicom or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with Mobicom.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 10, 2011 4:27:36 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.ui.screens.provider;

import com.jmw.news.LocaleManager;
import com.jmw.news.gen.L10nConstants;
import com.jmw.ui.foundation.provider.BaseScreenProvider;
import com.jmw.updater.Updater;
import com.jmw.utils.VectorUtils;

/**
 * Internal Class
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class AboutFrameProvider extends BaseScreenProvider {

  private String applicationNameLabel = LocaleManager
      .getString(L10nConstants.keys.APPLICATION_NAME);
  private String versionLabel = LocaleManager
      .getString(L10nConstants.keys.VERSION)
      + " "
      + Updater.getVersion();
  private String companyLabel = "Mobicom Co.,LTD";
  private String websiteLabel = "www.mobicom.vn";

  public final static String REF_APPLICATION_NAME =
      "ref_aplication_name_label";
  public final static String REF_VERSION_LABEL = "ref_version_label";
  public final static String REF_COMPANY_LABEL = "ref_company_label";
  public final static String REF_WEBSITE_LABEL = "ref_website_label";
  public final static String REF_SENTENCES = "ref_sentences";

  protected Object getUserDefinedValue(String ref) {
    return getUserDefinedValue(
        ref,
        VectorUtils
            .fromArray(new String[] { REF_APPLICATION_NAME,
                REF_VERSION_LABEL, REF_COMPANY_LABEL,
                REF_WEBSITE_LABEL }),
        VectorUtils.fromArray(new String[] { applicationNameLabel,
            versionLabel, companyLabel, websiteLabel }));
  }
}
