/**
 * Copyright (C) 2011 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * Mobicom or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with Mobicom.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 17, 2011 9:29:39 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.ui.screens;

import java.util.Hashtable;
import java.util.Vector;

import javax.microedition.lcdui.Image;

import la.pandora.mobile.logging.Logger;
import la.pandora.mobile.logging.SystemLogger;
import la.pandora.mobile.util.TaskCallback;
import la.pandora.mobile.util.TaskOperator;
import la.pandora.mobile.util.TaskResult;

import org.kalmeo.kuix.core.Kuix;
import org.kalmeo.kuix.widget.List;
import org.kalmeo.kuix.widget.ListItem;
import org.kalmeo.kuix.widget.Picture;

import com.jmw.foundation.controller.JMWController;
import com.jmw.foundation.controller.impl.JMWControllerImpl;
import com.jmw.foundation.task.GetImageTask;
import com.jmw.foundation.task.TaskUtils;
import com.jmw.news.LocaleManager;
import com.jmw.news.gen.L10nConstants;
import com.jmw.news.ui.screens.provider.PictureProvider;
import com.jmw.news.ui.screens.provider.PicturesFrameProvider;
import com.jmw.ui.foundation.provider.BaseListScreenProvider;
import com.jmw.ui.foundation.screens.BaseListFrame;
import com.jmw.utils.DelayedTask;
import com.jmw.utils.SysUtils;
import com.jmw.utils.ThreadUtils;

/**
 * Internal Class
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class PicturesFrame extends BaseListFrame {

  /**
   * Internal Class
   * 
   * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
   * 
   */
  public class GetImageCallback implements TaskCallback {

    public void taskCallback(TaskResult result) {
      if (provider != null) {
        if (TaskUtils.finishProcessFromServer(result
            .getResultStatus())) {
          imageIndex++;
          if (imageIndex < pictureUrls.size()) {
            logger.info("get imageIndex =" + imageIndex);
            logger.info("get image ="
                + (String) pictureUrls.elementAt(imageIndex));
            jmwControler.getImage(
                (String) pictureUrls.elementAt(imageIndex),
                new GetImageCallback());
          } else {
            ((BaseListScreenProvider) provider)
                .setLoadingVisible(false);
          }

        }
        if (result.getResultStatus() == TaskOperator.STATUS_SUCCESS) {
          GetImageTask task = (GetImageTask) result.getTask();
          image = task.getImage();
          renderView();
        }
      }
    }
  }

  Image image;

  public static final String EXTRA_IMAGE_URLS = "image_urls";
  private Vector pictureUrls;
  public static Logger logger = new SystemLogger("PicturesFrame");
  JMWController jmwControler;
  int imageIndex;

  private List pictures;

  public PicturesFrame(Hashtable bundle) {
    provider = new PicturesFrameProvider();
    jmwControler = new JMWControllerImpl();
    if (bundle != null) {
      pictureUrls = (Vector) bundle.get(EXTRA_IMAGE_URLS);
      imageIndex = 0;
      logger.info("get imageIndex =" + imageIndex);
      logger.info("get image ="
          + (String) pictureUrls.elementAt(imageIndex));

      jmwControler.getImage(
          (String) pictureUrls.elementAt(imageIndex),
          new GetImageCallback());
    }

  }

  public void onAdded() {
    logger.info("[onAdded] free memory="
        + Runtime.getRuntime().freeMemory());

    setScreen(Kuix.loadScreen(
        "/xml/jmw/news/pictures_frame.xml",
        provider));

    getScreen().setTitle(
        LocaleManager.getString(L10nConstants.keys.PICTURE));

    SysUtils.checkNotNull(new Object[] { getScreen(), provider });
    getScreen().setCurrent();

    pictures = (List) getScreen().getWidget("id_list_picture");
    renderView();
    ThreadUtils.runTaskDelayedAt(new DelayedTask() {
      public void run() {
        ((BaseListScreenProvider) provider).setLoadingVisible(true);
      }
    }, 500);

  }

  protected void chooseItem() {
    // TODO Auto-generated method stub

  }

  public void renderView() {
    if (image != null) {
      PictureProvider pictureProvider = new PictureProvider();
      provider.addItem(
          PicturesFrameProvider.REF_PICTURES,
          pictureProvider);
      ListItem item =
          (ListItem) pictures.getItemWidget(pictureProvider);
      Picture picture = (Picture) item.getWidget("id_picture");
      picture.setImage(image);
    }
    super.renderView();
  }

  public void onRemoved() {
    image = null;
    provider = null;
    jmwControler = null;
    pictureUrls = null;
    pictures.cleanUp();
    pictures = null;
    screen.cleanUp();
    screen = null;
    super.onRemoved();
  }
}
