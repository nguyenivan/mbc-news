/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * MOBICOM or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with MOBICOM.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 2, 2011 2:09:23 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.ui.screens.provider;

import com.jmw.news.foundation.model.NewsHeader;
import com.jmw.ui.foundation.provider.BaseProvider;
import com.jmw.utils.VectorUtils;

public class NewsHeaderProvider extends BaseProvider {

  public final static String TITLE_REF = "ref_title";
  public final static String DATE_REF = "ref_date";
  public final static String HEADER_REF = "ref_header";
  public final static String ICON_PATH_REF = "ref_icon_path";

  private String iconPath;
  private NewsHeader newsHeader;

  public NewsHeaderProvider(NewsHeader newsHeader, String iconPath) {
    this.newsHeader = newsHeader;
    this.iconPath = iconPath;
  }

  protected Object getUserDefinedValue(String ref) {
    return getUserDefinedValue(ref, VectorUtils
        .fromArray(new String[] { TITLE_REF, HEADER_REF,
            ICON_PATH_REF, DATE_REF }), VectorUtils
        .fromArray(new String[] { newsHeader.getTitle(),
            newsHeader.getHeader(), iconPath, newsHeader.getDate() }));
  }

  public void setIconPath(String iconPath) {
    this.iconPath = iconPath;
  }

  public String getIconPath() {
    return iconPath;
  }

  public void setSong(NewsHeader newsHeader) {
    this.newsHeader = newsHeader;
  }

  public NewsHeader getNewsHeader() {
    return newsHeader;
  }
}
