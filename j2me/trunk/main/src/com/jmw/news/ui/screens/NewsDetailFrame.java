/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * MOBICOM or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with MOBICOM.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 4, 2011 2:36:09 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.ui.screens;

import java.util.Hashtable;

import javax.microedition.lcdui.Image;

import la.pandora.mobile.logging.Logger;
import la.pandora.mobile.logging.SystemLogger;
import la.pandora.mobile.util.TaskCallback;
import la.pandora.mobile.util.TaskOperator;
import la.pandora.mobile.util.TaskResult;

import org.kalmeo.kuix.core.Kuix;
import org.kalmeo.kuix.widget.Picture;
import org.kalmeo.kuix.widget.TextArea;
import org.kalmeo.util.StringTokenizer;

import com.jmw.foundation.controller.JMWController;
import com.jmw.foundation.controller.impl.JMWControllerImpl;
import com.jmw.foundation.task.GetImageTask;
import com.jmw.foundation.task.TaskUtils;
import com.jmw.news.LocaleManager;
import com.jmw.news.foundation.controller.ControllerProvider;
import com.jmw.news.foundation.model.News;
import com.jmw.news.foundation.model.NewsHeader;
import com.jmw.news.foundation.task.news.GetNewsContentTask;
import com.jmw.news.gen.L10nConstants;
import com.jmw.news.ui.screens.provider.NewsDetailFrameProvider;
import com.jmw.news.ui.screens.provider.ParagraphProvider;
import com.jmw.ui.foundation.provider.BaseListScreenProvider;
import com.jmw.ui.foundation.screens.BaseListFrame;
import com.jmw.ui.foundation.screens.FrameUtils;
import com.jmw.utils.HashtableUtils;
import com.jmw.utils.DelayedTask;
import com.jmw.utils.SysUtils;
import com.jmw.utils.ThreadUtils;

/**
 * Internal Class
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class NewsDetailFrame extends BaseListFrame {

  /**
   * Internal Class
   * 
   * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
   * 
   */
  public class GetImageCallback implements TaskCallback {

    public void taskCallback(TaskResult result) {
      if (result.getResultStatus() == TaskOperator.STATUS_SUCCESS
          && provider != null) {
        GetImageTask task = (GetImageTask) result.getTask();

        Image image = task.getImage();
        // In case: warrant onRemove() execute and below objects will
        // be null
        if (newsImage != null && provider != null && news != null) {
          newsImage.setImage(image);
          adsImage.setVisible(false);
          ((NewsDetailFrameProvider) provider).setPicSepVisible(true);
          if (news.getImageUrls().size() > 1) {
            ((NewsDetailFrameProvider) provider)
                .setSeeMorePicVisible(true);
          }
        }
      }
    }
  }

  /**
   * Internal Class
   * 
   * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
   * 
   */
  public class GetNewsContentCallback implements TaskCallback {

    public void taskCallback(TaskResult result) {
      if (provider != null) {
        if (TaskUtils.finishProcessFromServer(result
            .getResultStatus())) {
          logger.info("stop loading news content");
          ((BaseListScreenProvider) provider)
              .setLoadingVisible(false);
        }
        if (result.getResultStatus() == TaskOperator.STATUS_SUCCESS) {
          GetNewsContentTask task =
              (GetNewsContentTask) result.getTask();
          News serverNews = task.getNews();
          serverNews.setHeader(news.getHeader());
          if (serverNews.getImageUrls() != null
              && serverNews.getImageUrls().size() > 0) {
            jmwControler.getImage((String) serverNews
                .getImageUrls()
                .elementAt(0), new GetImageCallback());
          }

          news = serverNews;

          renderView();

        }
      }
    }
  }

  public static final String EXTRA_NAME_HEADER = "name_header";
  public static final Object EXTRA_TOPIC_NAME = "topic_name";
  private String topicName;
  private News news;
  private Picture newsImage;
  JMWController jmwControler;
  private TextArea newsSource;
  private Picture loadingImg;
  private Picture adsImage;

  public static Logger logger = new SystemLogger("NewsDetailFrame");

  /**
   * @param fromArray
   */
  public NewsDetailFrame(Hashtable bundle) {
    logger.info("NewsDetailFrame");
    provider = new NewsDetailFrameProvider();
    if (bundle != null) {
      NewsHeader newsHeader =
          (NewsHeader) bundle.get(EXTRA_NAME_HEADER);
      topicName = (String) bundle.get(EXTRA_TOPIC_NAME);
      news =
          new News(
              newsHeader.getId(),
              newsHeader.getTitle(),
              newsHeader.getFullHeader(),
              null, // image urls
              null, // content
              null, // source
              newsHeader.getDate()); // date
      ((NewsDetailFrameProvider) provider).setNews(news);
    }
    if (news.getId() != null) {
      logger.info("news.getId()=" + news.getId());

      ControllerProvider.getNewsController().getNewsContent(
          news.getId(),
          new GetNewsContentCallback());
    }

    jmwControler = new JMWControllerImpl();
  }

  protected void chooseItem() {
    // TODO Auto-generated method stub
  }

  public void onAdded() {
    logger.info("[onAdded] free memory="
        + Runtime.getRuntime().freeMemory());
    setScreen(Kuix.loadScreen(
        "/xml/jmw/news/news_detail_frame.xml",
        provider));
    getScreen().setTitle(topicName);
    SysUtils.checkNotNull(new Object[] { getScreen(), provider });
    getScreen().setCurrent();
    newsImage = (Picture) getScreen().getWidget("id_news_image");
    adsImage = (Picture) getScreen().getWidget("id_news_ads");
    newsSource = (TextArea) getScreen().getWidget("id_source");
    loadingImg = (Picture) getScreen().getWidget("id_loading");
    loadingImg.setVisible(true);
    renderView();
    ThreadUtils.runTaskDelayedAt(new DelayedTask() {
      public void run() {
        if (news.getContent() == null) {
          // ((BaseListScreenProvider)
          // provider).setLoadingVisible(true);
          Kuix.getCanvas().flushGraphics();

        }
      }
    }, 500);
    Kuix.getCanvas().setInteractionListener(this);
  }

  public void _tmp() {

  }

  public void renderView() {
    if (news != null) {
      if (news.getContent() != null) {
        StringTokenizer sTokens =
            new StringTokenizer(news.getContent(), "\n");
        while (sTokens.hasMoreElements()) {
          ParagraphProvider paragraphProvider =
              new ParagraphProvider(sTokens.nextToken());
          provider.addItem(
              NewsDetailFrameProvider.REF_PARAGRAPHS,
              paragraphProvider);
        }
      }
      if (news.getSource() != null) {
        newsSource.setText(LocaleManager
            .getString(L10nConstants.keys.FOLLOW)
            + " "
            + news.getSource());
        ((NewsDetailFrameProvider) provider)
            .setSourceSepVisible(true);
      }
    }
    super.renderView();
  }

  public void onRemoved() {
    provider = null;
    news.setHeader(null);
    news.setContent(null);
    news = null;
    super.onRemoved();
  }

  public boolean onMessage(Object identifier, Object[] arguments) {
    logger.info("onMessage=" + identifier);
    if ("see_more_pic".equals(identifier)) {
      FrameUtils.removeAndPushFrame(new PicturesFrame(HashtableUtils
          .fromArray(new Object[] { PicturesFrame.EXTRA_IMAGE_URLS,
              news.getImageUrls() })));
    }
    return super.onMessage(identifier, arguments);

  }
}
