/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * Mobicom or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with Mobicom.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 4, 2011 3:01:31 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.ui.screens.provider;

import org.kalmeo.util.BooleanUtil;

import com.jmw.news.LocaleManager;
import com.jmw.news.foundation.model.News;
import com.jmw.news.gen.L10nConstants;
import com.jmw.ui.foundation.provider.BaseListScreenProvider;
import com.jmw.utils.VectorUtils;

/**
 * Internal Class
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class NewsDetailFrameProvider extends BaseListScreenProvider {
  public final static String REF_TITLE = "ref_title";
  public final static String REF_HEADER = "ref_header";
  public final static String REF_DATE = "ref_date";
  public final static String REF_PICTURE_SEP_VISIBLE =
      "ref_picture_sep_visible";
  public final static String REF_SOURCE_SEP_VISIBLE =
      "ref_source_sep_visible";
  public final static String REF_SEE_MORE_PIC_VISIBLE =
      "ref_see_more_pic_visible";
  public final static String REF_SEE_MORE_PIC_LABEL =
      "ref_see_more_pic_label";

  public final static String REF_PARAGRAPHS = "ref_paragraphs";

  private News news;
  private boolean isPicSepVisible = false;
  private boolean isSourceSepVisible = false;
  private boolean isSeeMorePicVisible = false;
  private String seeMorePicLabel = LocaleManager
      .getString(L10nConstants.keys.SEE_MORE_PIC_LABEL);

  protected Object getUserDefinedValue(String ref) {
    return getUserDefinedValue(
        ref,
        VectorUtils.fromArray(new String[] { REF_TITLE, REF_HEADER,
            REF_DATE, REF_PICTURE_SEP_VISIBLE,
            REF_SOURCE_SEP_VISIBLE, REF_SEE_MORE_PIC_VISIBLE,
            REF_SEE_MORE_PIC_LABEL }),
        VectorUtils.fromArray(new String[] { news.getTitle(),
            news.getHeader(), news.getDate(),
            BooleanUtil.toString(isPicSepVisible),
            BooleanUtil.toString(isSourceSepVisible),
            BooleanUtil.toString(isSeeMorePicVisible),
            seeMorePicLabel }));
  }

  /**
   * @param news
   *          the news to set
   */
  public void setNews(News news) {
    this.news = news;
  }

  /**
   * @return the news
   */
  public News getNews() {
    return news;
  }

  /**
   * @param isPicSepVisible
   *          the isPicSepVisible to set
   */
  public void setPicSepVisible(boolean isPicSepVisible) {
    this.isPicSepVisible = isPicSepVisible;
    dispatchUpdateEvent(REF_PICTURE_SEP_VISIBLE);
  }

  /**
   * @return the isPicSepVisible
   */
  public boolean isPicSepVisible() {
    return isPicSepVisible;
  }

  /**
   * @param isSourceSepVisible
   *          the isSourceSepVisible to set
   */
  public void setSourceSepVisible(boolean isSourceSepVisible) {
    this.isSourceSepVisible = isSourceSepVisible;
    dispatchUpdateEvent(REF_SOURCE_SEP_VISIBLE);
  }

  /**
   * @return the isSourceSepVisible
   */
  public boolean isSourceSepVisible() {
    return isSourceSepVisible;
  }

  /**
   * @param isSeeMorePicVisible
   *          the isSeeMorePicVisible to set
   */
  public void setSeeMorePicVisible(boolean isSeeMorePicVisible) {
    this.isSeeMorePicVisible = isSeeMorePicVisible;
    dispatchUpdateEvent(REF_SEE_MORE_PIC_VISIBLE);
  }

  /**
   * @return the isSeeMorePicVisible
   */
  public boolean isSeeMorePicVisible() {
    return isSeeMorePicVisible;
  }
}
