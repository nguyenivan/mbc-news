/**
 * Copyright (C) 2011 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * Mobicom or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with Mobicom.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 9, 2011 9:03:04 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.ui.screens;

import la.pandora.mobile.logging.Logger;
import la.pandora.mobile.logging.SystemLogger;

import org.kalmeo.kuix.core.Kuix;

import com.jmw.news.LocaleManager;
import com.jmw.news.gen.L10nConstants;
import com.jmw.news.ui.screens.provider.AccountFrameProvider;
import com.jmw.ui.foundation.screens.BaseFrame;
import com.jmw.utils.SysUtils;

/**
 * Internal Class
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class AccountFrame extends BaseFrame {
  public static Logger logger = new SystemLogger("AccountFrame");

  public AccountFrame() {
    provider = new AccountFrameProvider();
  }

  public void onAdded() {
    logger.info("[onAdded] free memory="
        + Runtime.getRuntime().freeMemory());
    setScreen(Kuix.loadScreen(
        "/xml/jmw/news/account_frame.xml",
        provider));
    getScreen().setTitle(
        LocaleManager.getString(L10nConstants.keys.ACCOUNT));
    SysUtils.checkNotNull(new Object[] { getScreen(), provider });
    getScreen().setCurrent();
    Kuix.getCanvas().setInteractionListener(this);
  }

}
