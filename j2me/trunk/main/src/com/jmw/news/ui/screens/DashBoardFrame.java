/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * MOBICOM or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with MOBICOM.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 2, 2011 2:09:23 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */

package com.jmw.news.ui.screens;

import java.util.Vector;

import la.pandora.mobile.logging.Logger;
import la.pandora.mobile.logging.SystemLogger;
import la.pandora.mobile.util.TaskCallback;
import la.pandora.mobile.util.TaskOperator;
import la.pandora.mobile.util.TaskResult;

import org.kalmeo.kuix.core.Kuix;
import org.kalmeo.kuix.core.KuixConstants;
import org.kalmeo.kuix.widget.ListItem;
import org.kalmeo.kuix.widget.ScrollPane;

import com.jmw.foundation.task.TaskUtils;
import com.jmw.news.LocaleManager;
import com.jmw.news.NewsMIDlet;
import com.jmw.news.Prefs;
import com.jmw.news.ShowUpdateResultAlertImpl;
import com.jmw.news.foundation.controller.ControllerProvider;
import com.jmw.news.foundation.controller.DefaultValueFactory;
import com.jmw.news.foundation.model.Feature;
import com.jmw.news.foundation.model.NewsHeader;
import com.jmw.news.foundation.task.news.GetNewsHeadersTask;
import com.jmw.news.gen.L10nConstants;
import com.jmw.news.ui.screens.provider.DashBoardFrameProvider;
import com.jmw.news.ui.screens.provider.NewsHeaderProvider;
import com.jmw.news.ui.screens.utils.IconPath;
import com.jmw.share.SMSSharer;
import com.jmw.ui.foundation.provider.BaseListScreenProvider;
import com.jmw.ui.foundation.provider.IconProvider;
import com.jmw.ui.foundation.screens.BaseListFrame;
import com.jmw.ui.foundation.screens.FrameUtils;
import com.jmw.updater.Updater;
import com.jmw.utils.HashtableUtils;
import com.jmw.utils.SysUtils;

public class DashBoardFrame extends BaseListFrame {

  public static Logger logger = new SystemLogger("NewsFrame");

  public Vector firstPageNewsHeaders;

  /**
   * Internal Class
   * 
   * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
   * 
   */
  private class GetFirstPageNewsCallback implements TaskCallback {

    public void taskCallback(TaskResult result) {
      logger.info("loading first page news status="
          + result.getResultStatus());

      if (TaskUtils.finishProcessFromServer(result.getResultStatus())) {
        logger.info("stop loading first page news");
        ((BaseListScreenProvider) provider).setLoadingVisible(false);
      }
      if (result.getResultStatus() == TaskOperator.STATUS_SUCCESS) {
        GetNewsHeadersTask task =
            (GetNewsHeadersTask) result.getTask();
        firstPageNewsHeaders = task.getNewsHeaders();
        renderView();
      }
    }
  }

  private Vector features;

  private static final String YES_EXIT = "yes_exit";

  public boolean onMessage(Object identifier, Object[] arguments) {
    logger.info("onMessage=" + identifier);
    if ("yes_exit".equals(identifier)) {
      NewsMIDlet.getInstance().exit();
    } else if ("exit".equals(identifier)) {
      if (EXIT.equals(modeExitHomeButton)) {
        Kuix.alert(
            LocaleManager
                .getString(L10nConstants.keys.PROMPT_TO_EXIT),
            KuixConstants.ALERT_YES | KuixConstants.ALERT_NO,
            YES_EXIT,
            null);
      } else {
        changeToExitLabel();
        _tmp();
      }
    } else if ("send_to_friend".equals(identifier)) {
      new SMSSharer(
          NewsMIDlet.getInstance().getDisplay(),
          Kuix.getCanvas(),
          0,
          Prefs.getSendFriendMessageContent()).sendToFriends();
    } else if ("update".equals(identifier)) {
      Updater.start(
          NewsMIDlet.getInstance(),
          Prefs.getAppName(),
          true,
          true,
          new ShowUpdateResultAlertImpl());
    } else if ("about".equals(identifier)) {
      FrameUtils.removeAndPushFrame(new AboutFrame());
    }
    return super.onMessage(identifier, arguments);
  }

  /**
   * 
   */
  private void changeToExitLabel() {
    ((DashBoardFrameProvider) provider).setExitLabel(LocaleManager
        .getString(L10nConstants.keys.EXIT));
    modeExitHomeButton = EXIT;
  }

  ScrollPane scrollPane;

  public DashBoardFrame() {
    provider = new DashBoardFrameProvider();
    features = ControllerProvider.getNewsController().getFeatures();

    int nFP =
        ControllerProvider
            .getSettingController()
            .getNumberItemOfFirstPage() == -1 ? DefaultValueFactory.FIRST_PAGE_NEWS_PER_LOAD
            : ControllerProvider
                .getSettingController()
                .getNumberItemOfFirstPage();
    ControllerProvider.getNewsController().getNewsHeaders(
        "0",
        0,
        nFP,
        new GetFirstPageNewsCallback());
  }

  public void onAdded() {
    logger.info(Logger.SEP);
    logger.info("[onAdded] free memory="
        + Runtime.getRuntime().freeMemory());
    logger.info("Dimension: Width=" + Kuix.getCanvas().getWidth()
        + " Height=" + Kuix.getCanvas().getHeight());
    _tmp();
    renderFeatures();
    renderView();
    ((BaseListScreenProvider) provider).setLoadingVisible(true);
    /*ThreadUtils.runTaskDelayedAt(new IDelayedTask() {
      public void run() {
        if (firstPageNewsHeaders == null) {
          ((BaseListScreenProvider) provider).setLoadingVisible(true);
          
        }
      }
    },
        500);*/
    if (ControllerProvider.getSettingController().isReleaseRevision()) {
      if (ControllerProvider.getSettingController().isFirstRun()) {
        FrameUtils.removeAndPushFrame(new AboutFrame());
      }
    }

    Kuix.getCanvas().setInteractionListener(this);
  }

  /**
   * 
   */
  private void _tmp() {
    setScreen(Kuix.loadScreen(
        "/xml/jmw/news/dash_board_frame.xml",
        provider));

    getScreen().setTitle(
        LocaleManager.getString(L10nConstants.keys.APP_NAME));

    scrollPane = (ScrollPane) screen.getWidget("id_scroll_pane");

    SysUtils.checkNotNull(new Object[] { getScreen(), provider });
    getScreen().setCurrent();
  }

  public void onRemoved() {
    Kuix.getCanvas().setInteractionListener(null);
  }

  protected void chooseItem() {
    if (getCurrentWidget() instanceof ListItem) {
      ListItem listItem = (ListItem) getCurrentWidget();
      if (listItem.getDataProvider() instanceof IconProvider) {
        IconProvider iconProvider =
            (IconProvider) listItem.getDataProvider();
        if (iconProvider != null) {
          Feature feature = (Feature) iconProvider.getAttachedObj();
          if (feature != null) {
            if (feature.getType().equals(Feature.TOPIC)) {
              FrameUtils.removeAndPushFrame(new TopicFrame());
            } else if (feature.getType().equals(Feature.SETTING)) {
              FrameUtils.removeAndPushFrame(new SettingFrame());
            } else if (feature.getType().equals(Feature.ACCOUNT)) {
              FrameUtils.removeAndPushFrame(new AccountFrame());
            }
          }
        }
      } else {
        NewsHeaderProvider newsHeaderProvider =
            (NewsHeaderProvider) listItem.getDataProvider();
        if (newsHeaderProvider != null) {
          NewsHeader newsHeader = newsHeaderProvider.getNewsHeader();
          if (newsHeader != null) {
            FrameUtils
                .removeAndPushFrame(new NewsDetailFrame(
                    HashtableUtils.fromArray(new Object[] {
                        NewsDetailFrame.EXTRA_NAME_HEADER,
                        newsHeader,
                        NewsDetailFrame.EXTRA_TOPIC_NAME,
                        LocaleManager
                            .getString(L10nConstants.keys.FIRST_PAGE) })));
          }
        }
      }
    }
  }

  public void renderFeatures() {
    if (features != null) {
      provider
          .removeAllItems(DashBoardFrameProvider.REF_NEW_FEATURES);
      for (int i = 0; i < features.size(); i++) {
        Feature feature = (Feature) features.elementAt(i);
        IconProvider itemProvider =
            new IconProvider(
                feature.getName(),
                feature.getType(),
                IconPath.getPath(feature),
                feature);
        provider.addItem(
            DashBoardFrameProvider.REF_NEW_FEATURES,
            itemProvider);
      }
    }
  }

  public void renderView() {
    if (firstPageNewsHeaders != null) {
      provider
          .removeAllItems(DashBoardFrameProvider.REF_FIRST_PAGE_ITEMS);
      for (int i = 0; i < firstPageNewsHeaders.size(); i++) {
        NewsHeader header =
            (NewsHeader) firstPageNewsHeaders.elementAt(i);
        NewsHeaderProvider newsHeaderProvider =
            new NewsHeaderProvider(header, IconPath.getPath(header));
        provider.addItem(
            DashBoardFrameProvider.REF_FIRST_PAGE_ITEMS,
            newsHeaderProvider);
      }
    }
    super.renderView();
  }

  private final String EXIT = "exit";
  private final String HOME = "home";

  String modeExitHomeButton = EXIT;

  public void onKeyEvent(byte type, int keyCode, int kuixKeyCode) {
    // TODO Auto-generated method stub
    /*if (kuixKeyCode == KuixConstants.KUIX_KEY_DOWN
        || kuixKeyCode == KuixConstants.KUIX_KEY_UP) {
      ListItem listItem = (ListItem) getCurrentWidget();
      if (listItem.getDataProvider() instanceof NewsHeaderProvider) {
        ((DashBoardFrameProvider) provider)
            .setExitLabel(LocaleManager
                .getString(L10nConstants.keys.GO_TOP));
        modeExitHomeButton = HOME;
      } else {
        changeToExitLabel();
      }
    }*/
  }

  public void onPointerEvent(byte type, int x, int y) {
    // TODO Auto-generated method stub

  }

}
