/**
 * Copyright (C) 2011 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * Mobicom or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with Mobicom.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 9, 2011 9:03:52 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */
package com.jmw.news.ui.screens.provider;

import com.jmw.news.LocaleManager;
import com.jmw.news.gen.L10nConstants;
import com.jmw.ui.foundation.provider.BaseScreenProvider;
import com.jmw.utils.VectorUtils;

/**
 * Internal Class
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 * 
 */
public class AccountFrameProvider extends BaseScreenProvider {

  private String accountType =
      LocaleManager.getString(L10nConstants.keys.FREE_ACCOUNT);

  public final static String REF_ACCOUNT_TYPE_LABEL =
      "ref_account_type";

  protected Object getUserDefinedValue(String ref) {
    return getUserDefinedValue(
        ref,
        VectorUtils
            .fromArray(new String[] { REF_ACCOUNT_TYPE_LABEL }),
        VectorUtils.fromArray(new String[] { accountType }));
  }
}
