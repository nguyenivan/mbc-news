/**
 * Copyright (C) 2010 MOBICOM. All rights reserved.
 * 
 * 
 * This software is the confidential and proprietary information of
 * MOBICOM or one of its subsidiaries. You shall not disclose this
 * confidential information and shall use it only in accordance with
 * the terms of the license agreement or other applicable agreement
 * you entered into with MOBICOM.
 * 
 * MOBICOM MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. MOBICOM
 * SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR
 * ITS DERIVATIVES.
 * 
 * Mar 2, 2011 2:09:23 PM
 * 
 * @author H&#7890; VI&#7878;T L&#226;m (lam.ho.viet@gmail.com)
 */

package com.jmw.news.ui.screens.provider;

import com.jmw.news.LocaleManager;
import com.jmw.news.Refs;
import com.jmw.news.gen.L10nConstants;
import com.jmw.ui.foundation.provider.BaseListScreenProvider;
import com.jmw.utils.VectorUtils;

public class DashBoardFrameProvider extends BaseListScreenProvider {

  private String updateLabel = LocaleManager
      .getString(L10nConstants.keys.UPDATE_VERSION);
  private String exitLabel = LocaleManager
      .getString(L10nConstants.keys.EXIT);
  private String aboutLabel = LocaleManager
      .getString(L10nConstants.keys.ABOUT);
  private String prefsLabel = LocaleManager
      .getString(L10nConstants.keys.PREFERENCES);
  private String sendToFriendLabel = LocaleManager
      .getString(L10nConstants.keys.SEND_TO_FRIEND);
  private String firstPageLabel = LocaleManager
      .getString(L10nConstants.keys.FIRST_PAGE);

  /**
   * Reference to list of genre
   */
  public final static String REF_NEW_FEATURES = "ref_features";

  public final static String REF_FIRST_PAGE_ITEMS =
      "ref_first_page_items";
  public final static String REF_FIRST_PAGE_LABEL =
      "ref_first_page_label";

  protected Object getUserDefinedValue(String ref) {
    return getUserDefinedValue(
        ref,
        VectorUtils.fromArray(new String[] { Refs.UPDATE_LABEL,
            Refs.ABOUT_LABEL, Refs.EXIT_LABEL, Refs.PREFS_LABEl,
            Refs.SEND_TO_FRIENDS_LABEL, REF_FIRST_PAGE_LABEL }),
        VectorUtils
            .fromArray(new String[] { updateLabel, aboutLabel,
                exitLabel, prefsLabel, sendToFriendLabel,
                firstPageLabel }));
  }

  /**
   * @param exitLabel
   *          the exitLabel to set
   */
  public void setExitLabel(String exitLabel) {
    this.exitLabel = exitLabel;
    dispatchUpdateEvent(Refs.EXIT_LABEL);
  }
}
