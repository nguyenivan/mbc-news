# PHI\u00CAN B\u1EA2N 1.1.66
* T\u00EDnh n\u0103ng:
+ H\u1ED5 tr\u1EE3 qu\u1EA3ng c\u00E1o s\u1EA3n ph\u1EA9m trong khi ch\u1EDD \u0111\u1EE3i h\u00ECnh \u1EA3nh \u0111ang t\u1EA3i
* S\u1EEDa l\u1ED7i:
+ \u0110ang t\u1EA3i d\u1EEF li\u1EC7u t\u1EEB server nh\u01B0ng sau \u0111\u00F3 tho\u00E1t kh\u1ECFi m\u00E0n h\u00ECnh hi\u1EC7n h\u00E0nh s\u1EBD g\u00E2y nullpointer
+ L\u1ED7i kh\u00F4ng hi\u1EC3n th\u1ECB bi\u1EC3u t\u01B0\u1EE3ng \u0111ang t\u1EA3i \u1EDF m\u00E0n h\u00ECnh tin t\u1EE9c chi ti\u1EBFt
* L\u1ED7i c\u00F2n t\u1ED3n t\u1EA1i:
+ Giao di\u1EC7n ng\u01B0\u1EDDi d\u00F9ng c\u00F2n ch\u01B0a th\u00E2n thi\u1EC7n v\u1EDBi m\u1ED9t trang b\u00E1o
=====================
# PHI\u00CAN B\u1EA2N 1.1.30
* T\u00EDnh n\u0103ng:
+ Ph\u00E2n lo\u1EA1i b\u00E1o theo chuy\u00EAn m\u1EE5c
+ Xem danh s\u00E1ch c\u00E1c tin ng\u1EAFn trong m\u1ED9t chuy\u00EAn m\u1EE5c
+ Xem chi ti\u1EBFt tin t\u1EE9c
+ Hi\u1EC3n th\u1ECB h\u00ECnh \u1EDF \u0111\u1EA7u m\u1ED9t trang tin
+ H\u1ED7 tr\u1EE3 xem th\u00EAm h\u00ECnh \u1EA3nh \u1EDF trong m\u1ED9t trang tin
+ Trang nh\u1EA5t d\u00F9ng \u0111\u1EC3 hi\u1EC3n th\u1ECB c\u00E1c tin m\u1EDBi nh\u1EA5t
+ M\u00E0n h\u00ECnh c\u00E0i \u0111\u1EB7t d\u00F9ng \u0111\u1EC3 t\u00F9y ch\u1ECDn s\u1ED1 l\u01B0\u1EE3ng tin \u1EDF trang nh\u1EA5t, s\u1ED1 l\u01B0\u1EE3ng tin v\u1EAFn m\u1ED7i l\u1EA7n t\u1EA3i \u1EDF c\u00E1c chuy\u1EC3n m\u1EE5c, c\u00E0i \u0111\u1EB7t t\u1EF1 \u0111\u1ED9ng c\u1EADp nh\u1EADt
+ M\u00E0n h\u00ECnh t\u00E0i kho\u1EA3n d\u00F9ng \u0111\u1EC3 th\u00F4ng b\u00E1o lo\u1EA1i t\u00E0i kho\u1EA3n c\u1EE7a ng\u01B0\u1EDDi d\u00F9ng
+ H\u1ED7 tr\u1EE3 g\u1EEDi t\u1EB7ng b\u1EA1n b\u00E8 \u1EE9ng d\u1EE5ng b\u1EB1ng c\u00E1ch g\u1EEDi SMS
+ C\u1EADp nh\u1EADt t\u1EF1 \u0111\u1ED9ng khi c\u00F3 phi\u00EAn b\u1EA3n m\u1EDBi
+ H\u1ED7 tr\u1EE3 gzip d\u00F9ng \u0111\u1EC3 n\u00E9n d\u1EEF li\u1EC7u gi\u1EEFa client v\u00E0 server
+ H\u1ED7 tr\u1EE3 release note cho c\u00E1c version